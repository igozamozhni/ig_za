/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


window.addEventListener('DOMContentLoaded', init);

function init() {

    var d = document;
    var navItems = d.querySelectorAll('.main-link');
    var sectionEditor = d.querySelector('.editor');
    var title = d.querySelector('.maincontent__title');

    // Tabs Navigation

    navItems.forEach(function (el) {
        return el.addEventListener('click', function (e) {

            e.preventDefault();

            var targetLink = e.target;
            var currentLink = d.querySelector('.current-link');
            var textLink = targetLink.textContent;

            currentLink.classList.remove('current-link');
            targetLink.classList.add('current-link');

            if (textLink !== 'about') {
                sectionEditor.style.display = "none";
                title.textContent = textLink;
            } else {
                sectionEditor.style.display = "block";
                title.textContent = textLink;
            }
        });
    });

    var divsEditElement = d.querySelectorAll('.edit-contact');

    divsEditElement.forEach(function (el) {
        return el.addEventListener('click', function (e) {
            e.preventDefault();

            var targetEdit = e.target;
            var targetDiv = el;
            var valueOfEdir = '';
            var createdEditSection = d.querySelector('.edit-input');

            if (targetEdit.tagName === 'A' || targetEdit.tagName === 'I') {

                // generated dynamically edit content

                if (targetEdit.tagName === 'A') {
                    valueOfEdir = targetEdit.previousElementSibling.textContent;
                }

                if (targetEdit.tagName === 'I') {
                    valueOfEdir = targetEdit.parentNode.previousElementSibling.textContent;
                }

                // create new tags

                var section = d.createElement('section');
                section.classList.add('edit-input');
                var _title = d.createElement('h6');
                _title.appendChild(d.createTextNode('CITY, STATE & ZIP'));
                section.appendChild(_title);
                var input = d.createElement('input');
                input.value = valueOfEdir;
                section.appendChild(input);
                var div = d.createElement('div');
                var buttonSave = d.createElement('button');
                buttonSave.classList.add('btn');
                buttonSave.setAttribute('id', 'save');
                buttonSave.appendChild(d.createTextNode('Save'));
                div.appendChild(buttonSave);
                var buttonCancel = d.createElement('button');
                buttonCancel.classList.add('btn');
                buttonCancel.setAttribute('id', 'cancel');
                buttonCancel.appendChild(d.createTextNode('Cancel'));
                div.appendChild(buttonCancel);
                section.appendChild(div);

                if (createdEditSection) {
                    var parentDiv = createdEditSection.parentNode;
                    parentDiv.removeChild(createdEditSection);
                }
                targetDiv.appendChild(section);

                var btnCancel = d.querySelector('#cancel');
                btnCancel.addEventListener('click', function () {
                    return targetDiv.removeChild(section);
                });

                var btnSave = d.querySelector('#save');
                btnSave.addEventListener('click', function (e) {
                    var inputData = e.target.parentNode.previousElementSibling.value;
                    targetDiv.childNodes[1].textContent = inputData;
                    targetDiv.removeChild(section);
                });
            }
        });
    });

    var buttonPhoneEdit = d.querySelector('.edit-phone');
    var divTitleWrap = d.querySelector('.title-wrap');

    buttonPhoneEdit.addEventListener('click', function (e) {
        e.preventDefault();
        var divEditor = d.querySelector('.editor');
        var fullName = divEditor.childNodes[1].childNodes[1].textContent;

        var pos = fullName.indexOf(' ');

        var firstName = fullName.slice(0, pos);
        var lastName = fullName.slice(pos, fullName.length);
        var website = divEditor.childNodes[3].childNodes[1].textContent;
        var phoneNumber = divEditor.childNodes[5].childNodes[1].textContent;
        var city = divEditor.childNodes[7].childNodes[1].textContent;

        divEditor.style.position = 'relative';

        var divNewEditor = d.createElement('div');
        divNewEditor.setAttribute('class', 'form-wrap');

        var labelFirstName = d.createElement('label');
        labelFirstName.textContent = 'FIRST NAME';
        divNewEditor.appendChild(labelFirstName);
        var inputFirstName = d.createElement('input');
        inputFirstName.value = firstName;
        divNewEditor.appendChild(inputFirstName);

        var labelLastName = d.createElement('label');
        labelLastName.textContent = 'LAST NAME';
        divNewEditor.appendChild(labelLastName);
        var inputLastName = d.createElement('input');
        inputLastName.value = lastName;
        divNewEditor.appendChild(inputLastName);

        var labelWebSite = d.createElement('label');
        labelWebSite.textContent = 'WEBSITE';
        divNewEditor.appendChild(labelWebSite);
        var inputWebSite = d.createElement('input');
        inputWebSite.value = website;
        divNewEditor.appendChild(inputWebSite);

        var labelPhone = d.createElement('label');
        labelPhone.textContent = 'PHONE NUMBER';
        divNewEditor.appendChild(labelPhone);
        var inputPhone = d.createElement('input');
        inputPhone.value = phoneNumber;
        divNewEditor.appendChild(inputPhone);

        var labelCity = d.createElement('label');
        labelCity.textContent = 'CITY, STATE & ZIP';
        divNewEditor.appendChild(labelCity);
        var inputCity = d.createElement('input');
        inputCity.value = city;
        divNewEditor.appendChild(inputCity);

        buttonPhoneEdit.style.display = 'none';

        var btnSave = d.createElement('button');
        btnSave.textContent = 'Save';
        btnSave.classList.add('btn');
        // btnSave.setAttribute('id', 'btnSave');

        var btnCancel = d.createElement('button');
        btnCancel.textContent = 'Cancel';
        btnCancel.classList.add('btn');
        // btnCancel.setAttribute('id', 'btnCancel');

        divTitleWrap.appendChild(btnSave);
        divTitleWrap.appendChild(btnCancel);

        divEditor.appendChild(divNewEditor);

        btnCancel.addEventListener('click', function () {
            divEditor.removeChild(divNewEditor);
            divTitleWrap.removeChild(btnSave);
            divTitleWrap.removeChild(btnCancel);
            buttonPhoneEdit.style.display = 'block';
        });

        btnSave.addEventListener('click', function () {
            var name = inputFirstName.value + ' ' + inputLastName.value;
            var site = inputWebSite.value;
            var phone = inputPhone.value;
            var cit = inputCity.value;

            divEditor.childNodes[1].childNodes[1].textContent = name;
            divEditor.childNodes[3].childNodes[1].textContent = site;
            divEditor.childNodes[5].childNodes[1].textContent = phone;
            divEditor.childNodes[7].childNodes[1].textContent = cit;

            divEditor.removeChild(divNewEditor);
            divTitleWrap.removeChild(btnSave);
            divTitleWrap.removeChild(btnCancel);
            buttonPhoneEdit.style.display = 'block';
        });
    });
}

/***/ })
/******/ ]);