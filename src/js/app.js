window.addEventListener('DOMContentLoaded', init);

function init () {

    const d = document;
    const navItems = d.querySelectorAll('.main-link');
    const sectionEditor = d.querySelector('.editor');
    const title = d.querySelector('.maincontent__title');

    // Tabs Navigation

    navItems.forEach( el => el.addEventListener ('click', e => {

        e.preventDefault();

        let targetLink = e.target;
        let currentLink = d.querySelector('.current-link');
        let textLink = targetLink.textContent;

        currentLink.classList.remove('current-link');
        targetLink.classList.add('current-link');
        
        if (textLink !== 'about') {
        sectionEditor.style.display = "none";
        title.textContent = textLink;
        } else {
            sectionEditor.style.display = "block";
            title.textContent = textLink;
        }
    }));


    const divsEditElement = d.querySelectorAll('.edit-contact');

    divsEditElement.forEach( el => el.addEventListener('click', e => {
        e.preventDefault();

        let targetEdit = e.target;
        let targetDiv = el;
        let valueOfEdir = '';
        const createdEditSection = d.querySelector('.edit-input');
        
        if(targetEdit.tagName === 'A' || targetEdit.tagName === 'I') {

        // generated dynamically edit content
            
            if(targetEdit.tagName === 'A') {
                valueOfEdir = targetEdit.previousElementSibling.textContent;
            }

            if(targetEdit.tagName === 'I') {
                valueOfEdir = targetEdit.parentNode.previousElementSibling.textContent;
            }

            // create new tags

            const section = d.createElement('section');
            section.classList.add('edit-input');
            const title = d.createElement('h6');
            title.appendChild(d.createTextNode('CITY, STATE & ZIP'));
            section.appendChild(title);
            const input = d.createElement('input');
            input.value = valueOfEdir;
            section.appendChild(input);
            const div = d.createElement('div');
            const buttonSave = d.createElement('button');
            buttonSave.classList.add('btn');
            buttonSave.setAttribute('id', 'save');
            buttonSave.appendChild(d.createTextNode('Save'));
            div.appendChild(buttonSave);
            const buttonCancel = d.createElement('button');
            buttonCancel.classList.add('btn');
            buttonCancel.setAttribute('id', 'cancel');
            buttonCancel.appendChild(d.createTextNode('Cancel'));
            div.appendChild(buttonCancel);
            section.appendChild(div);

            if(createdEditSection) {
                const parentDiv = createdEditSection.parentNode;
                parentDiv.removeChild(createdEditSection);
            }
            targetDiv.appendChild(section); 

            const btnCancel = d.querySelector('#cancel');
            btnCancel.addEventListener('click', () => targetDiv.removeChild(section));

            const btnSave = d.querySelector('#save');
            btnSave.addEventListener('click', (e) => {
                let inputData = e.target.parentNode.previousElementSibling.value;
                targetDiv.childNodes[1].textContent = inputData;
                targetDiv.removeChild(section);
            });
        }
    }));

    const buttonPhoneEdit = d.querySelector('.edit-phone');
    const divTitleWrap = d.querySelector('.title-wrap');

    buttonPhoneEdit.addEventListener('click', (e) => {
        e.preventDefault();
        const divEditor = d.querySelector('.editor');
        const fullName = divEditor.childNodes[1].childNodes[1].textContent;

        let pos = fullName.indexOf(' ');

        const firstName = fullName.slice(0, pos);
        const lastName = fullName.slice(pos, fullName.length);
        const website = divEditor.childNodes[3].childNodes[1].textContent;
        const phoneNumber = divEditor.childNodes[5].childNodes[1].textContent;
        const city = divEditor.childNodes[7].childNodes[1].textContent; 
        
        divEditor.style.position = 'relative';

        const divNewEditor = d.createElement('div');
        divNewEditor.setAttribute('class', 'form-wrap');

        const labelFirstName = d.createElement('label');
        labelFirstName.textContent = 'FIRST NAME';
        divNewEditor.appendChild(labelFirstName);
        const inputFirstName = d.createElement('input');
        inputFirstName.value = firstName;
        divNewEditor.appendChild(inputFirstName);

        const labelLastName = d.createElement('label');
        labelLastName.textContent = 'LAST NAME';
        divNewEditor.appendChild(labelLastName);
        const inputLastName = d.createElement('input');
        inputLastName.value = lastName;
        divNewEditor.appendChild(inputLastName);

        const labelWebSite = d.createElement('label');
        labelWebSite.textContent = 'WEBSITE';
        divNewEditor.appendChild(labelWebSite);
        const inputWebSite = d.createElement('input');
        inputWebSite.value = website;
        divNewEditor.appendChild(inputWebSite);

        const labelPhone = d.createElement('label');
        labelPhone.textContent = 'PHONE NUMBER';
        divNewEditor.appendChild(labelPhone);
        const inputPhone = d.createElement('input');
        inputPhone.value = phoneNumber;
        divNewEditor.appendChild(inputPhone);

        const labelCity = d.createElement('label');
        labelCity.textContent = 'CITY, STATE & ZIP';
        divNewEditor.appendChild(labelCity);
        const inputCity = d.createElement('input');
        inputCity.value = city;
        divNewEditor.appendChild(inputCity);

        buttonPhoneEdit.style.display = 'none';

        const btnSave = d.createElement('button');
        btnSave.textContent = 'Save';
        btnSave.classList.add('btn');
        // btnSave.setAttribute('id', 'btnSave');

        const btnCancel = d.createElement('button');
        btnCancel.textContent = 'Cancel';
        btnCancel.classList.add('btn');
        // btnCancel.setAttribute('id', 'btnCancel');

        divTitleWrap.appendChild(btnSave);
        divTitleWrap.appendChild(btnCancel);

        divEditor.appendChild(divNewEditor);

        btnCancel.addEventListener('click', () => {
            divEditor.removeChild(divNewEditor);
            divTitleWrap.removeChild(btnSave);
            divTitleWrap.removeChild(btnCancel);
            buttonPhoneEdit.style.display = 'block';
        });

        btnSave.addEventListener('click', () => {
            let name = `${inputFirstName.value} ${inputLastName.value}`;
            let site = inputWebSite.value;
            let phone = inputPhone.value;
            let cit = inputCity.value;

            divEditor.childNodes[1].childNodes[1].textContent = name;
            divEditor.childNodes[3].childNodes[1].textContent = site;
            divEditor.childNodes[5].childNodes[1].textContent = phone;
            divEditor.childNodes[7].childNodes[1].textContent = cit;

            divEditor.removeChild(divNewEditor);
            divTitleWrap.removeChild(btnSave);
            divTitleWrap.removeChild(btnCancel);
            buttonPhoneEdit.style.display = 'block';
        });
        

    })
}